﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using microservices_with_kafka.core;
using microservices_with_kafka.customer.Application.Commands;
using microservices_with_kafka.customer.Application.Infra.Repositories;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace microservices_with_kafka.customer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IUnitOfWork _uow;
        private readonly ICustomerRepository _customerRepository;

        public CustomersController(IMediator mediator, IUnitOfWork uow, ICustomerRepository customerRepository)
        {
            _mediator = mediator;
            _uow = uow;
            _customerRepository = customerRepository;
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IEnumerable<dynamic>>> Get()
        {
            return Ok(await _customerRepository.GetAll());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(string id)
        {
            return "value";
        }

        [HttpGet("{id}/mapreduce")]
        public async Task<ActionResult<IEnumerable<dynamic>>> GetMapReduce(string id)
        {
            return Ok(await _customerRepository.MapReduce());
        }

        // POST api/values
        [HttpPost]
        public async Task Post([FromBody] CustomerCreateCommand command)
        {
            _uow.StartSession();

            try
            {
                var result = await _mediator.Send(command);

                if (result.Valid == false)
                    _uow.Rollback();
                
            }
            catch (Exception e)
            {
                _uow.Rollback();
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task Put(string id, [FromBody] CustomerUpdateCommand command)
        {
            _uow.StartSession();

            try
            {
                var result = await _mediator.Send(command);

                if (result.Valid == false)
                    _uow.Rollback();

            }
            catch (Exception e)
            {
                _uow.Rollback();
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task Delete(string id)
        {
            _uow.StartSession();
            var command = new CustomerDeleteCommand();
            command.id = id;
            try
            {
                var result = await _mediator.Send(command);

                if (result.Valid == false)
                    _uow.Rollback();

            }
            catch (Exception e)
            {
                _uow.Rollback();
            }
        }
    }
}
