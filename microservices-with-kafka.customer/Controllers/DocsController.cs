﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace microservices_with_kafka.customer.Controllers
{
    [Route("docs")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class DocsController : Controller
    {
        [HttpGet, Route("")]
        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }
    }
}
