﻿using System.ComponentModel;

namespace microservices_with_kafka.customer.Application.Entities
{
    public enum EnumStatusWrokflow
    {
        [Description("Aguardando Aprovação")]
        AguardandoAprovacao,

        [Description("Aprovado")]
        Aprovado,

        [Description("Reprovado")]
        Reprovado
    }

    public enum EnumStatus
    {
        Ativo,
        Inativo,
        Bloqueado
    }

}