﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using microservices_with_kafka.core;

namespace microservices_with_kafka.customer.Application.Entities
{
    public class CustomerEntity : IEntity
    {
        public string id { get; set; }

        public string name { get; set; }

        public string middlename { get; set; }

        public string cellphone { get; set; }

        public string phone { get; set; }

        public int? age { get; set; }

        public EnumStatusWrokflow statusWorkflow { get; set; } = EnumStatusWrokflow.AguardandoAprovacao;

        public EnumStatus status { get; set; } = EnumStatus.Ativo;

    }
}
