﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using microservices_with_kafka.core;
using microservices_with_kafka.customer.Application.Entities;
using MediatR;

namespace microservices_with_kafka.customer.Application.Commands
{
    public class CustomerCreateCommand : IRequest<RequestResult<CustomerEntity>>
    {
        public string name { get; set; }
        public string middlename { get; set; }
        public string cellphone { get; set; }
        public string phone { get; set; }
        public int? age { get; set; }
        public EnumStatus status { get; set; }
    }

    public class CustomerUpdateCommand : IRequest<RequestResult<CustomerEntity>>
    {
        public string id { get; set; }
        public string name { get; set; }
        public string middlename { get; set; }
        public string cellphone { get; set; }
        public string phone { get; set; }
        public int? age { get; set; }
        public EnumStatusWrokflow statusWorkflow { get; set; } = EnumStatusWrokflow.AguardandoAprovacao;
        public EnumStatus status { get; set; } = EnumStatus.Ativo;
    }

    public class CustomerDeleteCommand : IRequest<RequestResult<bool>>
    {
        public string id { get; set; }
    }
}
