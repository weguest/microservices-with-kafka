﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using microservices_with_kafka.customer.Application.Entities;

namespace microservices_with_kafka.customer.Application.Infra.Repositories
{
    public interface ICustomerRepository
    {
        Task<IEnumerable<CustomerEntity>> GetAll();
        Task<IEnumerable<dynamic>> MapReduce();
        Task<CustomerEntity> CreateAsync(CustomerEntity entity);
        Task<CustomerEntity> UpdateAsync(CustomerEntity entity);
        Task<long> DeleteAsync(string id);

    }
}
