﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cassandra;
using microservices_with_kafka.core;
using microservices_with_kafka.customer.Application.Entities;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;

namespace microservices_with_kafka.customer.Application.Infra.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly IMongoDatabase _database;
        private readonly IUnitOfWork _uow;
        private readonly IMongoCollection<CustomerEntity> _collection;
 
        public CustomerRepository( IUnitOfWork uow)
        {

            if (BsonClassMap.IsClassMapRegistered(typeof(CustomerEntity)) == false)
            {
                BsonClassMap.RegisterClassMap<CustomerEntity>(cm =>
                {
                    cm.AutoMap();
                    cm.MapMember(c => c.status).SetSerializer(new EnumSerializer<EnumStatus>(BsonType.String));
                    cm.MapMember(c => c.statusWorkflow)
                        .SetSerializer(new EnumSerializer<EnumStatusWrokflow>(BsonType.String));
                });
            }

            _uow = uow;
            _database = _uow.Database();
            _collection = _database.GetCollection<CustomerEntity>("Customers");
            
        }

        public async Task<IEnumerable<CustomerEntity>> GetAll()
        {
            var list = await _collection.FindAsync(Builders<CustomerEntity>.Filter.Empty);
            return list.ToList();
        }

        public async Task<IEnumerable<dynamic>> MapReduce()
        {

            var map = @"function() {
                emit(this.status, 1);
            };";

            var reduce = @"function(status, num) {
                return Array.sum(num);
            };";

            var mr = await  _collection.MapReduceAsync<dynamic>(map, reduce);
            return mr.ToList();
        }

        public async Task<CustomerEntity> CreateAsync(CustomerEntity entity)
        {
            await _collection.InsertOneAsync(_uow.Session(), entity);
            return entity;
        }

        public async Task<CustomerEntity> UpdateAsync(CustomerEntity entity)
        {
            var filter = Builders<CustomerEntity>.Filter.Eq("id", entity.id);
            var result = await _collection.ReplaceOneAsync(_uow.Session(), filter, entity);
            return entity;
        }

        public async Task<long> DeleteAsync(string id)
        {
            var filter = Builders<CustomerEntity>.Filter.Eq("id", id);
            var result = await _collection.DeleteOneAsync(_uow.Session(), filter);
            return result.DeletedCount;
        }
    }
}
