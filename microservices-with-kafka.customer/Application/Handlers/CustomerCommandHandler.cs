﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using microservices_with_kafka.core;
using microservices_with_kafka.customer.Application.Commands;
using microservices_with_kafka.customer.Application.Entities;
using microservices_with_kafka.customer.Application.Infra.Repositories;
using MediatR;

namespace microservices_with_kafka.customer.Application.Handlers
{
    public class CustomerCommandHandler : 
        IRequestHandler<CustomerCreateCommand,RequestResult<CustomerEntity>>,
        IRequestHandler<CustomerUpdateCommand,RequestResult<CustomerEntity>>,
        IRequestHandler<CustomerDeleteCommand,RequestResult<bool>>
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerCommandHandler( ICustomerRepository customerRepository )
        {
            _customerRepository = customerRepository;
        }

        public async Task<RequestResult<CustomerEntity>> Handle(CustomerCreateCommand request, CancellationToken cancellationToken)
        {

            var entity = new CustomerEntity()
            {
                age = request.age,
                cellphone = request.cellphone,
                middlename = request.middlename,
                name = request.name,
                phone = request.phone,
                id = "CTS" + Guid.NewGuid().ToString().ToUpper().Substring(0, 12)
            };

            await _customerRepository.CreateAsync(entity);

            return new RequestResult<CustomerEntity>(){ Result = entity, Valid = true};

        }

        public async Task<RequestResult<CustomerEntity>> Handle(CustomerUpdateCommand request, CancellationToken cancellationToken)
        {
            var entity = new CustomerEntity()
            {
                id = request.id,
                status = request.status,
                statusWorkflow = request.statusWorkflow,
                age = request.age,
                cellphone = request.cellphone,
                middlename = request.middlename,
                name = request.name,
                phone = request.phone
            };

            await _customerRepository.UpdateAsync(entity);

            return new RequestResult<CustomerEntity>() { Result = entity, Valid = true };
        }

        public async Task<RequestResult<bool>> Handle(CustomerDeleteCommand request, CancellationToken cancellationToken)
        {
            var value  = await _customerRepository.DeleteAsync(request.id);
            return new RequestResult<bool>() { Result = value == 1, Valid = true };
        }
    }
}
