﻿using System;
using System.Collections.Generic;
using System.Text;

namespace microservices_with_kafka.core
{
    public class RequestResult<T>
    {
        public bool Valid { get; set; }
        public T Result { get; set; }
    }
}
