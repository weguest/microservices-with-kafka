﻿using System;

namespace microservices_with_kafka.core
{
    public interface IEntity
    {
        string id { get; set; }
    }
}
