﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Confluent.Kafka;
using MongoDB.Bson.IO;
using Newtonsoft.Json;
using JsonConvert = Newtonsoft.Json.JsonConvert;

namespace microservices_with_kafka.core
{
    public static class KafkaPublisher
    {

        static readonly AutoResetEvent _closing = new AutoResetEvent(false);

        static IProducer<string, string> _producer = null;
        static ProducerConfig _producerConfig = null;

        public static void CreateProducer()
        {
            _producerConfig = new ProducerConfig
            {
                BootstrapServers = "localhost:9092"
            };

            var pb = new ProducerBuilder<string, string>(_producerConfig);
            _producer = pb.Build();
        }

        public static async void Publish(object @event)
        {
            var msg = new Message<string, string>
            {
                Key = null,
                Value = JsonConvert.SerializeObject( @event )
            };

            var delRep = await _producer.ProduceAsync(@event.GetType().Name, msg);
            var topicOffset = delRep.TopicPartitionOffset;

            Console.WriteLine($"Delivered '{delRep.Value}' to: {topicOffset}");
        }

    }
}
