﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Driver;

namespace microservices_with_kafka.core
{
    public interface IUnitOfWork : IDisposable
    {
        void StartSession();

        void Commit();

        void Rollback();

        IMongoDatabase Database();

        IClientSessionHandle Session();
    }
}
