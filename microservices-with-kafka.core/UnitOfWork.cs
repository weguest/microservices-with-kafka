﻿using System;
using MongoDB.Driver;

namespace microservices_with_kafka.core
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IMongoDatabase _database;
        private IClientSessionHandle _session;

        public UnitOfWork(IMongoDatabase database)
        {
            _database = database;
        }

        public IMongoDatabase Database()
        {
            return _database;
        }

        public IClientSessionHandle Session()
        {
            return _session;
        }

        public void StartSession()
        {
            _session = _database.Client.StartSession();
        }

        public void Commit()
        {
            _session.CommitTransaction();
        }

        public void Rollback()
        {
            _session.AbortTransaction();
        }

        public void Dispose()
        {
            if (_session != null && _session.IsInTransaction == false)
            {
                GC.SuppressFinalize(this);
            }
        }

    }
}